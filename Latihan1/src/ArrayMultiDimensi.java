public class ArrayMultiDimensi {
    public static void main(String[] args){
        String mahasiswa[][]={
            {
                "021210009", "Anselmus"
            },
            {
                "021210049", "Shelly"
            },
            {
                "021210067", "Epa"
            },
            {
                "021210065", "Alham"
            },
            {
                "021210039", "Riki"
            }
        };
        System.out.println("Nama "+ mahasiswa[2][1]+" " +"Berada pada baris 3, kolom 2");
        System.out.println(mahasiswa[2][1]+" " +"Berada pada baris 3, kolom 2");
        System.out.println("NPM "+ mahasiswa[2][0]+" " +"Berada pada baris 3, kolom 1");
        
    }
    
}